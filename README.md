# DOCKERVINO

  DockerVINO = Docker + OpenVINO

## Introduction

Docker image compatible with Ubuntu 18.04, Ubuntu 20.04 and Fedora(tested with 33 but should work with others).

## Instructions

Since Openvino releases are quite frequent (2021.3 now), I choose to make the dockerfile public instead of the Docker images.

We have 4 images.

phase1 -- installs the Openvino SDK, launches a small benchmark and downloads  various dl models and converts them.
Requires: 
* [Docker_ci](https://github.com/openvinotoolkit/docker_ci) image or
* [DockerHub](https://hub.docker.com/u/openvino) image

demo   -- reduces space, adds script to run examples for Kontron

benchmark -- adds different traditional networks (vgg, resnet, inception) for benchmarking.

debug  -- adds debugging tools to the image to use with open_model_zoo repo

You need to build phase1, demo, benchmark and at last debug
```sh
   bash buildcmd
 ```

### Launch benchmark
In order to build benchmark and launch it, you need at least phase1.
Modify runcmd depending on CPU/MYRIAD/GPU
 ```sh 
   cd benchmark
   bash buildcmd
   bash runcmd
 ```

Any help/advice is appreciated at juan-diego.gonzales-zuniga@kontron.com

### Demo
In order to launch a demo, you need phase1. 
 ```sh 
   cd demo
   bash buildcmd
   bash tracker | face | ssd | yolo | humanpose3d
 ```