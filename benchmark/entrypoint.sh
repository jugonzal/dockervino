#!/bin/bash
# setupvars defined in runcmd (necessary for the same bash session)
source $setupvars

# app, device, model and input are defined in the runcmd file
benchmark=/root/inference_engine_samples_build/intel64/Release/benchmark_app
models=/root/openvino_models
echo "Benchmarking starts now!"
lscpu

echo "Testing: Inception V3"
$benchmark -d $device -api $api -pc -m $models/public/googlenet-v3/$datatype/googlenet-v3.xml

echo "Testing: Inception V4"
$benchmark -d $device -api $api -pc -m $models/public/googlenet-v4-tf/$datatype/googlenet-v4-tf.xml

echo "Testing: VGG16"
$benchmark -d $device -api $api -pc -m $models/public/vgg16/$datatype/vgg16.xml

echo "Testing: VGG19"
$benchmark -d $device -api $api -pc -m $models/public/vgg19/$datatype/vgg19.xml

echo "Testing: Resnet 50"
$benchmark -d $device -api $api -pc -m $models/public/resnet-50-tf/$datatype/resnet-50-tf.xml

echo "Testing: Efficientnet B0"
$benchmark -d $device -api $api -pc -m $models/public/efficientnet-b0-pytorch/$datatype/efficientnet-b0-pytorch.xml

echo "Testing: Efficientnet B5"
$benchmark -d $device -api $api -pc -m $models/public/efficientnet-b5-pytorch/$datatype/efficientnet-b5-pytorch.xml

echo "Testing: Efficientnet B7"
$benchmark -d $device -api $api -pc -m $models/public/efficientnet-b7-pytorch/$datatype/efficientnet-b7-pytorch.xml

echo "Testing: I3D"
$benchmark -d $device -api $api -pc -m $models/public/i3d-rgb-tf/$datatype/i3d-rgb-tf.xml

echo "Benchmarking done!"

