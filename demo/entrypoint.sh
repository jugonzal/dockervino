#!/bin/bash
# setupvars defined in runcmd (necessary for the same bash session)
source $setupvars
export PYTHONPATH="$PYTHONPATH:/root/omz_demos_build/intel64/Release/lib"
### If face detection then apply MYRIAD DEVICE

# app, device, model and input are defined in the runcmd file
echo "Device used: $device"
echo "Application used: $app"
echo "Model used: $model"


if [ "$app" = "/root/omz_demos_build/intel64/Release/interactive_face_detection_demo" ]; then
	$app -i $input -d $device -m $model -d_ag $device -m_ag $agM -d_em $device -m_em $emM -d_hp $device -m_hp $hpM 
elif [ "$app" = "/root/omz_demos_build/intel64/Release/human_pose_estimation_demo" ]; then
	$app -i $input -at openpose -d $device -m $model -send $send -broker $broker -client $client
elif [ "$app" = "/root/omz_demos_build/intel64/Release/object_detection_demo" ]; then
	$app -i $input -at $detection -d $device -m $model
elif [ "$app" = "/root/omz_demos_build/intel64/Release/pedestrian_tracker_demo" ]; then
        $app -i $input -d_det $device -d_reid $device -m_det $m_det -m_reid $m_reid -send $send -broker $broker -client $client
elif [ "$app" = "/opt/intel/openvino/deployment_tools/open_model_zoo/demos/human_pose_estimation_3d_demo/python/human_pose_estimation_3d_demo.py" ]; then
	if [ "$send" = "true" ]; then
		python3 $app -i $input -d $device -m $model --send --broker $broker --client $client
	else
		python3 $app -i $input -d $device -m $model 
	fi

elif [ "$app" = "/opt/intel/openvino/deployment_tools/open_model_zoo/demos/instance_segmentation_demo/python/instance_segmentation_demo.py" ]; then
	python3 $app -i $input -d $device -m $model --labels $labels_file
else 
	$app -i $input -d $device -m $model
fi

